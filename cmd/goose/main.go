package main

import (
	"bitbucket.org/arthurbrown/goose/lib/command"
)

func main() {
	command.Run()
}
